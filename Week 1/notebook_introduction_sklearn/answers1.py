print("Keys of the iris_dataset: \n{}".format(iris_dataset.keys()))
print(iris_dataset['DESCR'][:193] + "\n...")

print("Target names: {}".format(iris_dataset['target_names']))
print("Feature names: \n{}".format(iris_dataset['feature_names']))
print("Type of data: {}".format(type(iris_dataset['data'])))

print("Shape of data: {}".format(iris_dataset['data'].shape))

print("First five rows of data:\n{}".format(iris_dataset['data'][:5]))

print("Type of target: {}".format(type(iris_dataset['target'])))

print("Shape of target: {}".format(iris_dataset['target'].shape))

print("Target:/n{}".format(iris_dataset['target']))
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(iris_dataset['data'], iris_dataset['target'], random_state=0)
print("X_train shape: {}".format(X_train.shape))
print("y_train shape: {}".format(y_train.shape))

print("X_test shape: {}".format(X_test.shape))
print("y_test shape: {}".format(y_test.shape))
from sklearn.neighbors import KNeighborsClassifier
knn = KNeighborsClassifier(n_neighbors=1)
knn.fit(X_train, y_train)
X_new = np.array([[5, 2.9, 1, 0.2]])
print("X_new.shape: {}".format(X_new.shape))

prediction = knn.predict(X_new)
print("Prediction: {}".format(prediction))
print("Predicted target name: {}".format(iris_dataset['target_names'][prediction]))
y_pred = knn.predict(X_test)
print("Test set predictions:\n {}".format(y_pred))
print("Test set score: {:.2f}".format(np.mean(y_pred == y_test)))

print("Test set score: {:.2f}".format(knn.score(X_test, y_test)))
import numpy as np
import matplotlib.pyplot as plt
%matplotlib inline
import pandas as pd
from sklearn.datasets import fetch_california_housing
california_dataset = fetch_california_housing()
print(california_dataset.DESCR)
from sklearn.model_selection import train_test_split
X_train, X_test, y_train, y_test = train_test_split(
    california_dataset['data'], california_dataset['target'], random_state=0)
california_df = pd.DataFrame(X_train,columns=california_dataset.feature_names)
california_df.hist(figsize=(15, 15))
plt.show()
california_df = pd.DataFrame(X_train,columns=california_dataset.feature_names)
california_df[['HouseAge','Population']].hist(figsize=(6, 3))
plt.show()
from sklearn.preprocessing import StandardScaler
californiaS_df = pd.DataFrame(StandardScaler().fit_transform(X_train),columns=california_dataset.feature_names)
californiaS_df[['HouseAge','Population']].hist(figsize=(6,3))
plt.show()
from sklearn.neighbors import KNeighborsRegressor
from sklearn.metrics import mean_absolute_error
knn = KNeighborsRegressor(n_neighbors=1)
knn.fit(X_train,y_train)
y_pred = knn.predict(X_test)
MAE = mean_absolute_error(y_test, y_pred) #lower is better
print(MAE)
from sklearn.preprocessing import StandardScaler
scaler = StandardScaler()
X_trainS = scaler.fit_transform(X_train)
X_testS = scaler.transform(X_test)

knn = KNeighborsRegressor(n_neighbors=1)
knn.fit(X_trainS, y_train)
y_pred = knn.predict(X_testS)
MAE = mean_absolute_error(y_test, y_pred) #lower is better
print(MAE)
from sklearn.pipeline import Pipeline
scaledknn = Pipeline([('scaler', StandardScaler()),
                      ('knn', KNeighborsRegressor(n_neighbors=1))])
scaledknn.fit(X_train, y_train)
y_pred = scaledknn.predict(X_test)
MAE = mean_absolute_error(y_test, y_pred) #lower is better
print(MAE)

Markdown example. With latex $y = x^2$ and all what you need. 
* It is always best to first try to solve the problems yourself. 
* Note that the answers usually just provide one solution. It is completely fine to find your own solution.

**Make sure that you keep the directory structure with all the files as provided**

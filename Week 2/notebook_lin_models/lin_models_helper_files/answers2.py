modelRidge = Pipeline([('poly', PolynomialFeatures(degree=3)), 
                  ('ridge', linear_model.Ridge(fit_intercept=False))])
modelRidge.fit(x, y)
modelRidge.predict(x)
np.random.seed(1337)
X = np.arange(0, 1, 0.1)
y = 3*np.cos(2*np.pi*X) + np.random.randn(10)

X_plot = np.arange(0, 1, 0.01)
plt.scatter(X, y, label="samples")
plt.plot(X_plot, 3*np.cos(2*np.pi*X_plot), label="y true", color='green')
plt.xlabel('x')
plt.ylabel('y')
plt.legend()


poly_lr = Pipeline([('poly', PolynomialFeatures(degree=3)),
              ('linear', LinearRegression(fit_intercept=False))])
poly_lr.fit(X.reshape(10, 1), y)
plot_predict = poly_lr.predict(X_plot.reshape(100, 1))
plt.scatter(X, y, label="samples")
plt.plot(X_plot, 3*np.cos(2*np.pi*X_plot), label="y true", color='green')
plt.plot(X_plot, plot_predict, color='orange', label="prediction")
plt.xlabel('x')
plt.ylabel('y')
plt.legend()

fig, axes = plt.subplots(2, 2, figsize=(10,8), sharey='col')
degrees = [0, 1, 3, 9]

for i, deg in enumerate(degrees):
    poly_lr.set_params(poly__degree=deg)
    poly_lr.fit(X.reshape(10, 1), y)
    plot_predict = poly_lr.predict(X_plot.reshape(100, 1))
    ax = axes[int(np.floor(i/2)), i%2]
    ax.set_ylim(bottom=-6, top=6)
    
    ax.scatter(X, y, label="samples")
    ax.plot(X_plot, 3*np.cos(2*np.pi*X_plot), label="y true", color='green')
    ax.plot(X_plot, plot_predict, label="predicted poly")
    ax.legend()
    ax.set(xlabel='x', ylabel='y')
    ax.set(title="Degree: {0}".format(deg))
    
plt.show()

# first question, added later
N= X.shape[0]    # fill in either 0 or 1
p= X.shape[1]    # fill in either 0 or 1
print("N: {0} \n".format(N))
print("p: {0} \n".format(p))


{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to Machine Learning\n",
    "## Homework set 2: Linear and polynomial models in sklearn\n",
    "\n",
    "Wim Wiegerinck\n",
    "revised August 30, 2022"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This homework set contains three sections:\n",
    "1. Linear Models (linear regression, ridge regression)\n",
    "2. Polynomials in sklearn (using preprocessing and pipelines)\n",
    "3. Fitting and overfitting"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1 Linear models in sklearn\n",
    "\n",
    "NB: texts and figures are for a large part taken from the Scikit-learn website: <a href=https://scikit-learn.org/stable/modules/linear_model.html>linear models</a> \n",
    "\n",
    "Study the following sections about linear regression, ridge regression and polynomials in scikit-learn. Be aware that notation below differs slightly from Bishop's notation.\n",
    "\n",
    "| Bishop | scikit-learn |\n",
    "| :-: | :-: |\n",
    "| $y$ | $\\hat{y}$|\n",
    "| $t$ | $y$ |\n",
    "| $\\lambda$ | $\\alpha$ |\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 1.1 Linear models\n",
    "The following are a set of methods for regression in which the target value is expected to be a linear combination of the features. in mathematical notation, if $\\hat{y}$ is the predicted value,\n",
    "\n",
    "$\\hat{y}(w, x) = w_0 + w_1 x_1 + ... + w_p x_p$\n",
    "\n",
    "Across the module, we designate the vector $w=(w_1, ..., w_p)$ as \n",
    "`coef_` and $w_0$ as `intercept_` ."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.1.1. Ordinary least squares\n",
    "<a href=https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html#sklearn.linear_model.LinearRegression>LinearRegression</a> fits a linear model with coefficients $w = (w_1, ..., w_p)$ to minimize the residual sum of squares between the observed targets in the dataset, and the targets predicted by the linear approximation. Mathematically it solves a problem of the form:\n",
    "\n",
    "$\\min_{w}||Xw - y ||^2_2$\n",
    "\n",
    "(Be aware of the vector/matrix and $L_2$ norm notation that is used:\n",
    "\n",
    "$||X w - y||_2^2 = \\sum_{n=1}^N (\\sum_{i=0}^{p} X_{ni}w_{i} - y_n)^2$ \n",
    "\n",
    "where $n$ runs over the $N$ examples and $i$ runs over the $p$ components of the vector $w$. $X_{ni}$ is the $i$-th component of the $n$-th input example. $y_n$ is the target of the $n$-th example.)\n",
    "\n",
    "<img src=\"./lin_models_helper_files/lin_reg.png\">\n",
    "\n",
    "<a href=https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html#sklearn.linear_model.LinearRegression> LinearRegression</a> will take in its `fit` method arrays $X, y$ and will store coefficients $w$ of the linear model in its `coef_` member:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn import linear_model\n",
    "reg = linear_model.LinearRegression()\n",
    "reg.fit([[0, 0], [1, 1], [2, 2]], [0, 1, 2])\n",
    "reg.coef_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is a second <a href=https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html>example</a>:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from sklearn.linear_model import LinearRegression\n",
    "X = np.array([[1, 1], [1, 2], [2, 2], [2, 3]])\n",
    "y = np.dot(X, np.array([1, 2])) + 3\n",
    "reg = LinearRegression().fit(X, y)\n",
    "print(\"score: {0} \\n\".format(reg.score(X, y)))\n",
    "print(\"reg coefficients: {0} \\n\".format(reg.coef_))\n",
    "print(\"reg intercept: {0} \\n\".format(reg.intercept_))\n",
    "print(\"prediction: {0}\".format(reg.predict(np.array([[3, 5]]))))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the notation of the book and the slides:\n",
    "\n",
    "$y = w_0 + w_1 x_1 + w_2 x_2$\n",
    "\n",
    "the intercept corresponds with $w_0$ and the coefficients with $w_1$ and $w_2$.\n",
    "\n",
    "---\n",
    "Let us have a closer look ath the solution of linear regression. We will do that with the use of Numpy’s linear algebra module. Let us take the following simple data set:\n",
    "\n",
    "---"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X = np.array([[1, 2], [2, 2], [3, 3], [1, 1]])\n",
    "y = np.array([1, 1.5, 3, 0.5])\n",
    "print(\"X: {0} \\n\".format(X))\n",
    "print(\"y: {0} \\n\".format(y))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "What is the number of samples (= number of data vectors = $N$) in this data? What is the number of features (=the number of variables = $p$) in $X$? "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "N= X.shape[...]    # fill in either 0 or 1\n",
    "p= X.shape[...]    # fill in either 0 or 1\n",
    "print(\"N: {0} \\n\".format(N))\n",
    "print(\"p: {0} \\n\".format(p))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load -r 48-51 ./lin_models_helper_files/answers2.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Make a copy of X with an additional zeroth column to model the dummy feature $x_0 = 1$ so that the linear model takes the form\n",
    "\n",
    "$y = w_0 x_0 + w_1 x_1 + w_2 x_2$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_b = np.c_[(np.ones((len(X), 1)), X)]\n",
    "print(X_b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The linear regression coefficients are given by normal equations:\n",
    "\n",
    "$\\mathbf{w} = (\\mathbf{X^{T} X})^{\\mathbf{-1}} \\mathbf{X^{T} y}$\n",
    "\n",
    "(in which $\\mathbf{X}$ should be interpreted as the extended design matrix X_b)\n",
    "With numpy's linear algebra functions (and @ for matrix multiplication) this can be coded as:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "w = np.linalg.inv(X_b.T @ X_b) @ X_b.T @ y\n",
    "print(w)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compare the solution $\\mathbf{w}$ with"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "LR = LinearRegression()\n",
    "LR.fit(X, y)\n",
    "LR.intercept_, LR.coef_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.1.2 Ridge regression\n",
    "\n",
    "\n",
    "<a href=https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Ridge.html#sklearn.linear_model.Ridge>Ridge</a> regression addresses some of the problems of <a href=https://scikit-learn.org/stable/modules/linear_model.html#ordinary-least-squares>Ordinary Least Squares</a> by imposing a penalty on the size of the coefficients. The ridge coefficients minimize a penalized residual sum of squares:\n",
    "\n",
    "$\\min_w ||Xw - y||_2^2 + \\alpha ||w||_2^2$\n",
    "\n",
    "The complexity parameter $\\alpha > 0$ controls the amount of shrinkage: the larger the value of $\\alpha$, the greater the amount of shrinkage and thus the coefficients become more robust to colinearity¹\n",
    "\n",
    "¹: $\\alpha$ is the weight decay. Note that Bishop uses $\\lambda$ as complexity parameter. Colinearity may be interpreted as overfitting in the polynomial case discussed in the lecture and Bishop's book. Note that in the graph, the bigger $\\alpha$'s are on the left. With bigger $\\alpha$, weights are more shrinked and therefore smaller. \n",
    "\n",
    "<img src=./lin_models_helper_files/ridgeregressionweights.png>\n",
    "\n",
    "As with other linear models, <a href=https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.Ridge.html#sklearn.linear_model.Ridge>Ridge</a> will take in its `fit` method arrays $X, y$ and will store the coefficients $w$ of the linear model in its `coef_` member:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn import linear_model\n",
    "reg = linear_model.Ridge(alpha=.5)\n",
    "reg.fit([[0, 0], [0, 0], [1, 1]], [0, .1, 1])\n",
    "print('reg.coef_ =', reg.coef_)\n",
    "print('reg.intercept_=', reg.intercept_)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the hand-in assignment 1 (later) you are asked to reproduce some of the results in Bishop section 1.1. which is about underfitting and overfitting in polynomial regression and the effect of regularization.\n",
    "\n",
    "The polynomial model is not part of sklearn. However, it is easily constructed using a Pipeline as the following text shows:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## 1.1.18. Polynomial regression: extending linear models with basic functions\n",
    "\n",
    "One common pattern within machine learning is to use linear models trained on nonlinear functions of the data. This approach maintains the generally fast performance of linear methods, while allowing them to fit a much wider range of data.\n",
    "\n",
    "For example, a simple linear regression can be extended by constructiong **polynomial features** from the coefficients. In the standard linear regression case, you might have a model that looks like this for two-dimensional data:\n",
    "\n",
    "$\\hat{y}(w, x) = w_0 + w_1 x_1 + w_2 x_2$\n",
    "\n",
    "If we want to fit a paraboloid to the data instead of a plane, we can combine the features in the second-order polynomials, so that the model looks like this:\n",
    "\n",
    "$\\hat{y}(w, x) = w_0 + w_1 x_1 + w_2 x_2 + w_3 x_1 x_2 + w_4 x_1^2 + w_5 x_2^2$\n",
    "\n",
    "The (sometimes surprising) observation is that this is *still a linear model*: to see this, imagine creating a new set of features\n",
    "\n",
    "$z = [x_1, x_2, x_1 x_2, x_1^2, x_2^2]$\n",
    "\n",
    "With this re-labeling of the data, our problem can be written\n",
    "\n",
    "$\\hat{y}(w, z) = w_0 + w_1 z_1 + w_2 z_2 + w_3 z_3 + w_4 z_4 + w_5 z_5$\n",
    "\n",
    "We see that the resulting *polynomial regression* is in the same class of linear models we considered above (i.e. the model is linear in $w$) and can be solved by the same techniques. By considering linear fits within a higher-dimensional space built with these basis functions, the model has the flexibility to fit a much broader range of data.\n",
    "\n",
    "Here is an example of applying this idea to one-dimensional data, using polynomial features of varying degrees:\n",
    "\n",
    "<img src=./lin_models_helper_files/polynomials.png>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This figure is created using the <a href=https://scikit-learn.org/stable/modules/generated/sklearn.preprocessing.PolynomialFeatures.html#sklearn.preprocessing.PolynomialFeatures>PolynomialFeatures</a> transformer, which transforms an input data matrix into a new data matrix of a given degree. It can be used as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.preprocessing import PolynomialFeatures\n",
    "import numpy as np\n",
    "\n",
    "X = np.arange(6).reshape(3, 2)\n",
    "X\n",
    "\n",
    "poly = PolynomialFeatures(degree=2)\n",
    "poly.fit_transform(X)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The features of $X$ have been transformed from $[x_1, x_2]$ to $[1, x_1, x_2, x_1^2, x_1 x_2, x_2^2]$, and can now be used within any linear model. \n",
    "\n",
    "This sort of preprocessing can be streamlined with the <a href=https://scikit-learn.org/stable/modules/compose.html#pipeline>Pipeline</a> tools. A single object representing a simple polynomial regression can be created and used as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.preprocessing import PolynomialFeatures\n",
    "from sklearn.linear_model import LinearRegression\n",
    "from sklearn.pipeline import Pipeline\n",
    "import numpy as np\n",
    "model = Pipeline([('poly', PolynomialFeatures(degree=3)), \n",
    "                  ('linear', LinearRegression(fit_intercept=False))])\n",
    "x = np.arange(5)\n",
    "y = 3 - 2 * x + x ** 2 - x ** 3\n",
    "model = model.fit(x[:, np.newaxis], y)\n",
    "model.named_steps['linear'].coef_"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The linear model trained on the polynomial features is able to exactly recover the input polynomial coefficients."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here is another code snippet, which applies the transformer to a 1-D variable `X` with values $[0, 0.1, ..., 0.9, 1]$. The `reshape(-1, 1)` is to put $X$ in the standard `n_samples` $\\times$ `n_features` shape."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from sklearn.preprocessing import PolynomialFeatures\n",
    "X = np.linspace(0, 1, 11).reshape(-1, 1)\n",
    "poly = PolynomialFeatures(degree=3)\n",
    "print(poly.fit_transform(X))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Question**: In the example code:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = Pipeline([('poly', PolynomialFeatures(degree=3)), \n",
    "                  ('linear', LinearRegression(fit_intercept=False))])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "why is `fit_intercept` set to `False`?\n",
    "\n",
    "try to answer this question, if you don't know, remove \\# in front of the code in the next cell and run to display an answer"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %run ./lin_models_helper_files/answersmd.py\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "So, the bottom line here is that we can easiliy build polynomial models by combining PolynomialFeatures with a linear mode (E.g. LinearRegression or Ridge)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "model = Pipeline([('poly', PolynomialFeatures(degree=3)), \n",
    "                  ('linear', LinearRegression(fit_intercept=False))])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Which can directly be used for fitting and predicting:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "x = np.arange(5)\n",
    "x = x[:, np.newaxis]\n",
    "y = - 2 * x + x ** 2 - x ** 3\n",
    "model.fit(x, y)\n",
    "model.predict(x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It works the same way with ridge regression if we want regularization by weight decay. Create a pipeline like the one above but now using ridge regression instead of linear regression:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "modelRidge = Pipeline([...])\n",
    "modelRidge.fit(...)\n",
    "modelRidge.predict(...)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load -r 1-4 ./lin_models_helper_files/answers2.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### More about Estimators, Pipelines and Parameters\n",
    "and some pitfalls"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The following code snippets should clarify how parameters in pipelines are accessed. Just execute the code blocks and have a look at the outcomes. These code snippets should also remind not to forget that Python works with obects and assignment by reference!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from sklearn.linear_model import LinearRegression\n",
    "from sklearn.linear_model import Ridge\n",
    "from sklearn.preprocessing import PolynomialFeatures\n",
    "from sklearn.pipeline import Pipeline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# just some 1-data, further details not important here\n",
    "X_train = np.linspace(0,1,10)\n",
    "y_train = np.sin(X_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "poly_lr = Pipeline([('poly', PolynomialFeatures()),\n",
    "              ('linear', LinearRegression(fit_intercept=False))])\n",
    "poly_lr"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the pipeline you can set the parameters of one if its members (using name ('poly') , a double underscore ('__') and followed by parametername=parametervalue ('degree=4')\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "poly_lr.set_params(poly__degree=4)\n",
    "\n",
    "poly_lr"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "poly_ridge = Pipeline([('poly', PolynomialFeatures(degree=9)),\n",
    "              ('ridge', Ridge(fit_intercept=False))])\n",
    "poly_ridge.set_params(ridge__alpha=0.01)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "poly_lr.fit(X_train.reshape(-1,1), y_train)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#(fit expects a NxM matrix, therefore reshape of X_train is required)\n",
    "print(X_train.shape, X_train.reshape(-1,1).shape)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "poly_lr.predict(X_train.reshape(-1,1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "NOTE: be aware of the effect of applying methods, doing assignments etc."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "poly_lr6 = poly_lr.set_params(poly__degree=6)\n",
    "poly_lr7 = poly_lr.set_params(poly__degree=7)\n",
    "poly_lr7.set_params(poly__degree=8)\n",
    "print(poly_lr6, poly_lr7, poly_lr)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Overfitting"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Suppose we have made some measurements and obtained some data points. Unfortunately, there was some imprecision in our measurements (as is usually the case) and the samples could not be obtained exactly from the underlying distribution.\n",
    "\n",
    "Set a random seed using `numpy.random.seed(a)` where `a` can be any constant you want. Defining a random seed at the start of your program will ensure that the same random numbers are generated if you rerun your code, thus everything is reproducible. Generate 10 equally spaced samples from the function:\n",
    "\n",
    "$y = 3\\cos(2 \\pi x)$\n",
    "\n",
    "Add some noise to these samples generated by a Gaussian with mean $0$ and variance $1$:\n",
    "\n",
    "$\\epsilon \\sim N(0, 1)$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "import numpy as np\n",
    "from sklearn.linear_model import LinearRegression\n",
    "from sklearn.preprocessing import PolynomialFeatures\n",
    "from sklearn.pipeline import Pipeline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "np.random.seed(...)\n",
    "X = np.arange(...)\n",
    "y = ... + np.random.randn(...)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load -r 5-7 ./lin_models_helper_files/answers2.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Plot the sampled data points using a scatterplot. Add the underlying distribution (create a more fine-grained domain $X_\\text{plot}$ to make a smooth plot of the underlying distribution)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_plot = ...\n",
    "plt.scatter(..., label=\"samples\")\n",
    "plt.plot(..., label=\"y true\", color='green')\n",
    "plt.xlabel('x')\n",
    "plt.ylabel('y')\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load -r 9-14 ./lin_models_helper_files/answers2.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Create a pipeline to make polynomial features of degree $3$ and then perform linear regression on these features (you may use code form the section above). You can plot the resulting function by predicting on $X_\\text{plot}$. Add the result to your previous plot.\n",
    "\n",
    "The input data has to be two-dimensional. You can use the function reshape(N, 1) to reshape a one-dimensional vector into a two-dimensional matrix."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "poly_lr = ...\n",
    "poly_lr.fit(X.reshape(10, 1), ...)\n",
    "plot_predict = ...\n",
    "plt.scatter(..., label=\"samples\")\n",
    "plt.plot(..., label=\"y true\", color='green')\n",
    "plt.plot(..., color='orange', label=\"prediction\")\n",
    "plt.xlabel('x')\n",
    "plt.ylabel('y')\n",
    "plt.legend()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load -r 17-26 ./lin_models_helper_files/answers2.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Repeat this process with polynomial orders $[0, 1, 3, 9]$ and create a figure similar to figure 1.4 in Bishop. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, axes = plt.subplots(2, 2, figsize=(10,8), sharey='col')\n",
    "degrees = [0, 1, 3, 9]\n",
    "\n",
    "for i, deg in enumerate(degrees):\n",
    "    poly_lr.set_params(...)\n",
    "    poly_lr.fit(...)\n",
    "    plot_predict = ...\n",
    "    ax = axes[int(np.floor(i/2)), i%2]\n",
    "    ax.set_ylim(bottom=-6, top=6)\n",
    "    \n",
    "    ax.scatter(X, y, label=\"samples\")\n",
    "    ax.plot(X_plot, 3*np.cos(2*np.pi*X_plot), label=\"y true\", color='green')\n",
    "    ax.plot(..., label=\"predicted poly\")\n",
    "    ax.legend()\n",
    "    ax.set(xlabel='x', ylabel='y')\n",
    "    ax.set(title=\"Degree: {0}\".format(deg))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# %load -r 28-46 ./lin_models_helper_files/answers2.py"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As the degree of the polynomial increases, the polynomial gains more expressive power. This means it can more precisely fit the points in the training set. Wit a degree of $9$, a polynomial can fit any $10$ points exactly, and there is no error on the training set. \n",
    "\n",
    "However, remember that the samples contained noise. The samples never followed the cosine distribution precisely. As the degree of the polynomial increases, it is capable of finding a very different function from the cosine that follows the sampled data much closer. But that function performs very poorly when confronted with new, unseen, samples. This is called overfitting.\n",
    "\n",
    "In machine learning, it is important to always use a separate training set and test set. The training set is used to fit the parameters of your model. Once you have found satisfying parameters, the performance should be tested using the test set.  Furthermore, as a rule of thumb it is generally best to not have the degrees of freedom in your machine learning algorithm exceed (or come close to) the number of data points in your data set (unless these are taken care of by some regularization method, in that case the rule of thumb may not apply). Finally, overfitting is usually accompanied by large weight values. A penalty term adding some cost to having large weights could also help prevent overfitting (for instance in ridge regression)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "\n",
    "Make sure that you understand:\n",
    "\n",
    "How polynomial curve fitting with a standard error function and with a regularized error\n",
    "function can be modeled in sklearn with the use of a pipeline of polynomial features\n",
    "combined with linear regression or ridge regression.\n",
    "\n",
    "What overfitting is, when it happens and what can be done to prevent it."
   ]
  }
 ],
 "metadata": {
  "@webio": {
   "lastCommId": null,
   "lastKernelId": null
  },
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

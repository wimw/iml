Lambda = np.diag(lambdas)
LambdaInv = np.diag(1/lambdas)

#Check
# U.T@U = I                (C.37)  Definition orthogonal matrix
# U.T@A@U = Lambda         (C.42)
# U@Lambda@U.T = A         (C.43)
# U@LambdaInv@U.T = A^{-1} (C.44)

print("Equation (C.37) is {0}".format(np.allclose(U.T@U, np.eye(3))))
print("Equation (C.42) is {0}".format(np.allclose(U.T@A@U, Lambda)))
print("Equation (C.43) is {0}".format(np.allclose(U@Lambda@U.T, A)))
print("Equation (C.44) is {0}".format(np.allclose(U@LambdaInv@U.T, np.linalg.inv(A))))


# I make a second pdf, but now mean zeros and cov defined by the diagonal matrix with of lambdas
Lambda21 = np.diag([3,1])        # Diagonal matrix with eigenvalues
Sigma21 = U@Lambda21@U.T         # Rotated
LambdaI = np.diag([1.5, 1.5])    # Diagonal matrix with equal eigenvalues
pxy1 = multivariate_normal([0,0], Sigma21)   # New Gaussian pdf
pxy2 = multivariate_normal([0,0], Lambda21)   # New Gaussian pdf
pxy3 = multivariate_normal([0,0], LambdaI)   # New Gaussian pdf
# Prepare figure
fig = plt.figure(figsize=(10,5))
# the following is to prepare the meshgrid needed for the contour plot
dx = dy = 0.01
x11 = np.arange(-mybound, mybound, dx)
y11 = np.arange(-mybound, mybound, dy)
x11, y11 = np.meshgrid(x11, y11)
pos1 = np.dstack((x11, y11))
# First subplot of two subplots in a single row and two columns
ax=plt.subplot(1, 3, 1)  
ax.contour(x11, y11, pxy1.pdf(pos1), cmap='Reds')
set_axes(ax, mybound, [0,0])       # this function that I defined for equal axes

# Second subplot of two subplots in a single row and two columns
ax=plt.subplot(1, 3, 2) 
ax.contour(x11, y11, pxy2.pdf(pos1), cmap='Reds')
set_axes(ax, mybound, [0,0])       # this function that I defined for equal axes

# Second subplot of two subplots in a single row and two columns
ax=plt.subplot(1, 3 , 3) 
ax.contour(x11, y11, pxy3.pdf(pos1), cmap='Reds')
set_axes(ax, mybound, [0,0])       # this function that I defined for equal axes

plt.tight_layout()  # Use this function after making your subplots, to avoid playing around with the spacing
plt.show()

# Use Bishop 2.81 & 2.82 to compute mu_xGIVENyc Sigma_xGIVENyc from mu, Sigma and yc
Sigma_xGIVENyc = Sigma[0,0]-Sigma[0,1]/Sigma[1,1]*Sigma[1,0]
mu_xGIVENyc    = mu[0]+Sigma[0,1]/Sigma[1,1]*(yc-mu[1])

# and with these, define a Gaussian
p_xGIVENyc_Exact = multivariate_normal(mu_xGIVENyc, Sigma_xGIVENyc)

# and plot both on top of each other with BIG fatty lines to see them both
fig = plt.figure()
plt.plot(y,p_xGIVENyc_Numericalpdf, label='Numerical', linewidth=10)
plt.plot(y,p_xGIVENyc_Exact.pdf(y),label = 'Exact', linewidth=4)
plt.xlabel('$x$')
plt.ylabel('$p(x|y={})$'.format(yc))
plt.legend()
                            
                            
plt.show()

#Numerical marginalization
# I need to evaluate the pdf in all these points
#x1 = np.arange(-5, 5, 0.01)
#y1 = np.arange(-5, 5, 0.01)
#x1, y1 = np.meshgrid(x1,y1)
#pos = np.dstack((x1, y1))

# Marginalization is integration over y (axis=1)
#p_x_Numericalpdf = np.sum(pxy.pdf(pos),axis=1) * 0.01 

# Eact marginalization
# Use Bishop 2.98 to compute mu_x Sigma_x from mu, Sigma 
Sigma_x = Sigma[0,0]
mu_x    = mu[0]

# and with these, define a Gaussian
p_x_Exact = multivariate_normal(mu_x, Sigma_x)

# and plot both on top of each other with BIG fatty lines to see them both
fig = plt.figure()
plt.plot(x,p_x_Numericalpdf, label='Numerical', linewidth=10)
plt.plot(x,p_x_Exact.pdf(x),label = 'Exact', linewidth=4)
plt.xlabel('$x$')
plt.ylabel('$p(x)$')
plt.legend()
                                                        
plt.show()

# p(x)
px = multivariate_normal(0, 1)

# one time the generation of x
x = np.random.randn()

fig = plt.figure()
x1 = np.arange(-5, 5, 0.01)
plt.plot(x1,px.pdf(x1), label='$p(x)$', linewidth=1)
plt.plot(x,0, '*', label='$x_{truth}$')

# For three values of N the observations y and p(x|y)
for N in np.array([1,10,100]):
    yvec = x+np.random.randn(N,1)
    A = np.ones((N,1))
    L = np.eye(N)
    Lambda=1
    SigmaN = np.linalg.inv(Lambda + A.T@L@A)
    muN = SigmaN@A.T@L@yvec
    px_y = multivariate_normal(muN,SigmaN)
    plt.plot(x1,px_y.pdf(x1), label='$p(x|y), N={}$'.format(N))
plt.xlabel('x')
plt.legend()
plt.show()

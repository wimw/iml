Answer the following questions:
1. Look at panel 1 and panel 2. Which of the three learning rates gives the fastest convergence? Which the second fastest and which the slowest?
    1. **Answer**: $\eta_{opt}$ gives the fastest convergence. $\eta_{s}$ the second fastest and $\eta_{b}$ the slowest
2. Look at panel 3, 5 and 7. Describe for each panels the convergence in both $x$ and $y$ directions: 
    1. Is there undershooting or overshooting (or directly on target) in the $x$  direction? And what about the $y$ direction?
        1. **Answer** panel 3: $x$ overshooting, $y$ undershooting.
        1. **Answer** panel 5: $x$ little bit undershooting - almost on target, $y$ undershooting.
        1. **Answer** panel 7: $x$ overshooting, $y$ under shooting.
    2. In which direction ($x$ or $y$) is convergence the fastest, and in which the slowest? Is there a big difference? (or are they even equally fast - and if so how is that possible?) 
        1. **Answer** panel 3: $x$, $y$ equally fast (in the sense that the convergence rates are the same). The amount of overshooting in the $x$ directions is exaclty in balance with the amount of undershooting in the $y$ direction.
        1. **Answer** panel 5: $x$ little bit undershooting - almost direct on target - so very fast. $y$ is much slower.
        1. **Answer** panel 7: $x$ overshooting, $y$ under shooting. Here $y$ seems a to go a little bit faster. If we look at the end point, we see that it has a small deviation from the target in the x-direction. So indeed $x$ goes a little bit slower.    
3. Look at panel 4, 6 and 8. In each panel, i.e. for each $\eta$ you can see the convergence rate $r_n$ of the 2-d system  and the two 1-d rates, $r(\eta, \lambda_1)$ in the $x$ direction and $r(\eta, \lambda_2)$ in the $y$ direction. 
    1. Does a large value for $r$ imply a fast or a slow convergence?
        1. **Answer**: Large $r$ means slow convergence. Zero r is convergence in one step.
    2. In panel 6 and 8, the graph of $r_n$ quickly goes to one of the $r(\eta, \lambda)$s with the highest value. Why is that? Explain.  Relate to findings in panel 5 and 7 respectively.
        1. **Answer**: the overall convergence is dominated by the slowest direction, ie. with the highest r-value. 
        1. **Answer** panel 6: In panel 6 we see that r in the $x$-direction is small and r in the $y$-direction is large. The 2-d r jumps directly to the r in the $y$-direction. The small r in the $x$-direction corresponds to the allmost directly on target convergence in the $x$-direction in panel 5. Then, since $x \approx x^*$, the distance between $(x,y)$ and $(x^*,y^*)$ is almost equal to the distance between $y$ and $y*$. 
        1. **Answer** panel 8: In panel 8 we see that r in the $x$-direction is larger than in the $y$-direction. However they are both relatively large. Earlier we noted that convergence in the $x$ direction is indeed a bit slower than in the $y$ direction. Overall convergence is dominated by the slower convergence in the $x$ direction (the end point seems only to deviate in the $x$-direction) 
    3. In panel 4 there is only one line visible. Why is that? Explain and relate to finding in panel 4.
        1. **Answer**: all rates are exactly the same. Therefore the lines coincide. We already saw earlier in panel 4 that convergence rates are in balance with each other.
4. What is the  problem with gradient descent for multivariate function minimization (= minimization of functions of multiple variables, such as $g(x,y)$)
    1. **Answer**: if function-landscape contains elongated valeys, the learning rate should be small to prevent divergence in the steep directions. Th small learning rate makes convergence slow in the flat directions. Finding the optimal learning rate is delicate and may be hard in practice, since the trajectories are on the edge of diverging...  (why did I propose $\eta_s = 1.07 \eta_{opt}$ and not $\eta_s = 1.1 \eta_{opt}$, for instance?)
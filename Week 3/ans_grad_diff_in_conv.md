The function $g$ has the shape of a valley with a steep direction (the $x$ direction)
and a much more flat direction (the $y$ direction). With the small $\eta$ that we tested, 
the convergence in the steep $x$ direction is relatively fast: 
the distance to the optimum $d(x,x^*)$ 
is about 0 after 40 iterations, but in the flat $y$ direction 
it is slow. In 100 iterations the distance to the optimum still is $d(y,y^*) \approx 1$. 
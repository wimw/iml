Answers
1. In how many iterations does Newton's algorithm find the minimum? 
* In 1 iteration 
2. Why could this be expected?
* Newton's algorithm does in each iteration an  exact minimisation of the quadratic function that is obtained by second order Taylor expansion at the current point. Here this quadratic function is identical to the original function $g$ (since $g$ itself is quadratic), so $g$ is exactly minimised in 1 step.
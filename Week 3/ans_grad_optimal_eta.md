The equation of the optimal value of $\eta$ is 
* $\eta_{opt} = \frac{2}{\lambda_1 + \lambda_2}$

So 


* `eta_opt = 2/(11 + 1) = 1/6`

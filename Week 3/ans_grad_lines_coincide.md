
Since the relations $r^x_n = r(\eta, \lambda_1)$ and $r^y_n = r(\eta, \lambda_2)$ hold exactly (as shown in the math exercises), they are plotted on top of each other.

Note that if you see 5 lines, something went wrong. (Make sure that you didn't change `eta`.)
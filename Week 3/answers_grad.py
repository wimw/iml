
def g(X):
    return (11/2)*(X[0]-3)**2 + (1/2)*(X[1]+2)**2
    

# optimal point:
X_opt = np.array([3.0, -2.0])

# define gradient at point X:    
def grad(X):
    a = np.array([3.0, -2.0])
    lambdas = np.array([11, 1])
    return lambdas*(X-a)  

    
def grad_descent_algorithm(eta, max_iter = 100):
    
    # intial point:
    X = np.array([0.0,0.0])
    # optimal point
    X_opt = np.array([3.0, -2.0])
    
    # Track 
    
    X_visited = np.zeros((2, max_iter))
    distances = np.zeros(max_iter)
    fun_values = np.zeros(max_iter)
    
    # keep track of values for intial point (0-th iteration)
    X_visited[:,0] = X
    fun_values[0] = g(X)
    distances[0] = dist(X, X_opt)
    
    # start iteration
    for it in range(1, max_iter):
        # the step here: new X = (something with old X , eta etc.)
        X -= eta*grad(X)
        
        # and here keep track of things
        X_visited[:,it] = X
        fun_values[it] = g(X)
        distances[it] = dist(X, X_opt)
        
    return(X_visited, fun_values, distances)
    
####  
  
r_n = distances[1:] / distances[:-1]
rx_n = np.abs(X_visited[0,1:] - X_opt[0])/np.abs(X_visited[0,:-1] - X_opt[0])
ry_n = np.abs(X_visited[1,1:] - X_opt[1])/np.abs(X_visited[1,:-1] - X_opt[1])

lambda1 = 11
lambda2 = 1
r1 = np.abs(1-eta*lambda1) * np.ones_like(r_n)
r2 = np.abs(1-eta*lambda2) * np.ones_like(r_n)

# plot routines
plt.plot(r_n, label = '$r_n$')
plt.plot(rx_n, label = '$r^{x}_n$')
plt.plot(ry_n, label = '$r^{y}_n$')
plt.plot(r1, label = '$r(\eta, \lambda_1)$')
plt.plot(r2, label = '$r(\eta, \lambda_2)$')

plt.xlabel('n')
plt.ylabel('$r$')
plt.legend()
plt.show()

###

eta_opt = 1/6
eta_s = 0.5*eta_opt
eta_l = 1.07*eta_opt
eta_all = [eta_opt,eta_s,eta_l]
eta_names = ['$\eta_{opt}$','$0.5 \eta_{opt}$','$1.07 \eta_{opt}$']

# prepare dicts to put our data in
X_t = dict()
g_t=dict()
d_t = dict()

# run the algorithm for the three cases
for i,eta in enumerate(eta_all):
    X_t[i], g_t[i], d_t[i] = grad_descent_algorithm(eta, 100)

# prepare the plots
plt.figure(figsize=(8,15))

# plot the function values of the three runs
plt.subplot(4,2,1)
plt.title('panel 1: function values')
for i,eta_name in enumerate(eta_names):
    plt.semilogy(g_t[i], label = eta_name) 
plt.legend()
plt.xlabel('Iterations')
plt.ylabel('g')

# plot the distances of the three runs
plt.subplot(4,2,2)
plt.title('panel 2: distances')
for i,eta_name in enumerate(eta_names):
    plt.semilogy(d_t[i], label = eta_name) 
plt.legend()
plt.xlabel('Iterations')
plt.ylabel('d')

# prepare for the contour plots (in case x,y have been overwritten)
x = np.arange(-1, 7, 0.1)
y = np.arange(-6, 2, 0.1)
x, y = np.meshgrid(x, y)

# three rows of plots for the three runs
for i,eta_name in enumerate(eta_names):
    
    # contour plots and trajectories
    plt.subplot(4,2,3+2*i) 
    cont = plt.contour(x, y, g([x, y]))
    plt.title('panel '+str(3+2+i)+': '+eta_name)
    plt.plot(X_t[i][0,:], X_t[i][1,:],'b')
    plt.plot(X_t[i][0,1:-1], X_t[i][1,1:-1],'ob')
    plt.plot(X_t[i][0,0], X_t[i][1,0],'or', ms=5)
    plt.plot(X_t[i][0,-1], X_t[i][1,-1],'or', ms=15)
    plt.plot(X_opt[0], X_opt[1],'xg',mew=2, ms=10)
    plt.clabel(cont)
    plt.xlabel('x')
    plt.ylabel('y')
    plt.gca().set_aspect('equal', adjustable='box')
    
    # plots of the convergence rates
    plt.subplot(4,2,4+2*i) 
    plt.title('panel '+str(4+2+i)+': '+eta_name)
    r_n = d_t[i][1:]/d_t[i][:-1]
    lambda1 = 11
    lambda2 = 1
    r1 = np.abs(1-eta_all[i]*lambda1)*np.ones_like(r_n)
    r2 = np.abs(1-eta_all[i]*lambda2)*np.ones_like(r_n)
    plt.plot(r_n, label = '$r_n$')
    plt.plot(r1, label = '$r(\eta, \lambda_1)$')
    plt.plot(r2, label = '$r(\eta, \lambda_2)$')
    plt.ylim(0,1)
    plt.legend()
    
plt.tight_layout()

plt.show()


###

def H_inv(X):
    Hinv = np.array([[1/11, 0.0], [0.0, 1]])
    return Hinv


# An alternative code using H and inversion by numpy

def H_inv(X):
    H = np.array([[11, 0.0], [0.0, 1]])
    Hinv = np.linalg.inv(H)
    return Hinv


### 

def newton_algorithm(max_iter = 100):
    
    # intial point:
    X = np.array([0.0,0.0])
    # optimal point
    X_opt = np.array([3.0, -2.0])
    
    # Track 
    
    X_visited = np.zeros((2, max_iter))
    distances = np.zeros(max_iter)
    fun_values = np.zeros(max_iter)
    
    # keep track of values for intial point (0-th iteration)
    X_visited[:,0] = X
    fun_values[0] = g(X)
    distances[0] = dist(X, X_opt)
    
    # start iteration
    for it in range(1, max_iter):
        # the step here: new X = (something with old X , eta etc.)
        X -= H_inv(X) @ grad(X)
        
        # and here keep track of things
        X_visited[:,it] = X
        fun_values[it] = g(X)
        distances[it] = dist(X, X_opt)
        
    return(X_visited, fun_values, distances)  
    
    
    
X = np.array([0.0, 0.0])

d = dist(X, X_opt)
X_visited_Newton = np.zeros((2, maxiter))
distances_Newton = np.zeros(maxiter)

it = 0
distances_Newton[it] = d
X_visited_Newton[:,it] = X
while d > threshold and it < maxiter:
    it += 1
    H_inv = np.array([[1/11, 0.0], [0.0, 1]])
    X -= np.dot(H_inv, grad(X))
    d = dist(X, X_opt)
    distances_Newton[it] = d
    X_visited_Newton[:,it] = X
    
X_visited_Newton = X_visited_Newton[:,:it+1]
distances_Newton = distances_Newton[:it+1]